# Pokemon - Softplan

## Live Demo - link https://daniel12fsp.gitlab.io/softplan-pokemon

## Features implemented

- Search Pokemons
- Edit locally pokemon with Apollo Local Storage
- Infinite Scroll
- CI/CD by GitLab
- Storybook for component
- Apollo to data from GraphQL's endpoint

## Partial implemented

- Implemented some test

## About me

Go to linkedin - https://www.linkedin.com/in/daniel-fernandes-pereira/

## To build

yarn install
yarn build
