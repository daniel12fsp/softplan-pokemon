import { ALL_FIELDS_POKEMON } from "./pokemon";

export const resolvers = {
  Mutation: {
    savePokemon: (_, variables, { cache, getCacheKey }) => {
      const id = getCacheKey({ __typename: "Pokemon", id: variables.id });
      const pokemon = cache.readFragment({ fragment: ALL_FIELDS_POKEMON, id });

      const data = {
        ...pokemon,
        ...variables,
        weight: {
          minimum: variables.weightMinimum,
          maximum: variables.weightMaximum,
          __typename: "PokemonDimension",
        },
        height: {
          minimum: variables.heightMinimum,
          maximum: variables.heightMaximum,
          __typename: "PokemonDimension",
        },
      };
      cache.writeData({ id, data });
      return null;
    },
  },
};
