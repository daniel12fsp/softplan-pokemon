import { gql } from "apollo-boost";

export const ALL_FIELDS_POKEMON = gql`
  fragment AllPokemonFields on Pokemon {
    id
    number
    name
    weight {
      minimum
      maximum
    }
    height {
      minimum
      maximum
    }
    classification
    types
    resistant
    weaknesses
    fleeRate
    maxCP
    maxHP
    image
  }
`;

export const GET_POKEMONS = gql`
  query pokemons($first: Int!) {
    pokemons(first: $first) {
      ...AllPokemonFields
    }
  }
  ${ALL_FIELDS_POKEMON}
`;

export const GET_POKEMON = gql`
  query pokemon($id: String!) {
    pokemon(id: $id) {
      ...AllPokemonFields
    }
  }
  ${ALL_FIELDS_POKEMON}
`;

export const SAVE_POKEMON = gql`
  mutation SavePokemon(
    $id: Int!
    $name: String!
    $classification: String!
    $fleeRate: String!
    $heightMaximum: String!
    $heightMinimum: String!
    $maxCP: String!
    $maxHP: String!
    $weightMaximum: String!
    $weightMinimum: String!
  ) {
    savePokemon(
      id: $id
      name: $name
      classification: $classification
      fleeRate: $fleeRate
      heightMaximum: $heightMaximum
      heightMinimum: $heightMinimum
      maxCP: $maxCP
      maxHP: $maxHP
      weightMaximum: $weightMaximum
      weightMinimum: $weightMinimum
    ) @client
  }
`;
