import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect, HashRouter } from "react-router-dom";
import "../node_modules/reset-css/reset.css";
import { DescribePage } from "./container/DescribePage/DescribePage";
import { HomePage } from "./container/HomePage/HomePage";
import "./theme/theme.css";
import { resolvers } from "./graphQL/resolver";

import { persistCache } from "apollo-cache-persist";
import { Loading } from "./components";

export default function App() {
  const [client, setClient] = useState(undefined);
  useEffect(() => {
    const cache = new InMemoryCache();

    const client = new ApolloClient({
      uri: "https://graphql-pokemon.now.sh",
      cache,
      clientState: {
        defaults: {},
        resolvers: resolvers,
      },
    });

    persistCache({
      cache,
      storage: window.localStorage,
    }).then(() => setClient(client));
    return () => {};
  }, []);

  if (client === undefined) return <Loading />;
  return (
    <ApolloProvider client={client}>
      <HashRouter basename={"/"}>
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/pokemons/:id">
            <DescribePage />
          </Route>
          <Route path="/404" component={() => "Not implemented page"} />
          <Redirect to="/404" />
        </Switch>
      </HashRouter>
    </ApolloProvider>
  );
}
