// copied from https://github.com/upmostly/react-hooks-infinite-scroll/blob/master/src/useInfiniteScroll.js

import { useState, useEffect } from "react";
import debounce from "lodash.debounce";

export const useInfiniteScroll = (callback) => {
  const [isFetching, setIsFetching] = useState(false);

  useEffect(() => {
    function handleScroll() {
      if (
        window.innerHeight + window.scrollY < document.body.scrollHeight ||
        isFetching
      )
        return;
      setIsFetching(true);
    }
    const debouncedHandleScroll = debounce(handleScroll, 1000);
    window.addEventListener("scroll", debouncedHandleScroll);
    return () => {
      debouncedHandleScroll.cancel();
      window.removeEventListener("scroll", debouncedHandleScroll);
    };
  });

  useEffect(() => {
    if (!isFetching) return;
    callback();
    setIsFetching(false);
  }, [isFetching, callback]);

  return [isFetching, setIsFetching];
};
