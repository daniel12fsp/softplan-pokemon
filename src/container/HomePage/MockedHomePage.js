import React from "react";
import { useMockPokemon } from "../../mocks/useMockPokemon";
import { HomePage } from "./HomePage";
import { SUCCESS } from "../../constants";

export const MockedSuccessHomePage = (props = {}) => {
  const [pokemons, getMorePokemon] = useMockPokemon(20);
  return (
    <HomePage
      pokemonsRequest={{ type: SUCCESS, pokemons }}
      getMorePokemon={getMorePokemon}
      {...props}
    />
  );
};

// export const mockedLoadingHomePage = (props) => {
//   const [pokemons, getMorePokemon] = useMockPokemon(20);
//   return (
//     <PokemonList
//       pokemons={pokemons}
//       getMorePokemon={getMorePokemon}
//       {...props}
//     />
//   );
// };

// export const mockedErrorHomePage = (props) => {
//   const [pokemons, getMorePokemon] = useMockPokemon(20);
//   return (
//     <PokemonList
//       pokemons={pokemons}
//       getMorePokemon={getMorePokemon}
//       {...props}
//     />
//   );
// };
