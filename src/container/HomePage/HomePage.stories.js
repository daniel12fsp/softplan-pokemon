import React from "react";
import { MockedSuccessHomePage } from "./MockedHomePage";

export default { title: "HomePage" };

export const successState = () => <MockedSuccessHomePage />;
