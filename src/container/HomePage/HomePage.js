import React, { useState, useMemo } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Search, PokemonList, Loading } from "../../components";
import styles from "./HomePage.module.css";
import { useQuery } from "@apollo/react-hooks";
import { GET_POKEMONS } from "../../graphQL/pokemon";

function useGetMorePokemon() {
  const [ammount, setAmount] = useState(20);
  const [loadingMore, setLoadingMore] = useState(undefined);

  const { loading: firstLoading, error, data, fetchMore } = useQuery(
    GET_POKEMONS,
    {
      variables: {
        first: 20,
      },
    }
  );

  const getMorePokemon = () => {
    setLoadingMore(true);

    fetchMore({
      variables: {
        first: ammount + 20,
      },

      updateQuery: (previousResult, { fetchMoreResult, variables }) => {
        setAmount(variables.first);
        const newsPokemons = fetchMoreResult.pokemons.slice(
          previousResult.pokemons.length
        );
        setLoadingMore(false);
        return {
          pokemons: [...previousResult.pokemons, ...newsPokemons],
        };
      },
    });
  };

  return { firstLoading, loadingMore, error, data, getMorePokemon };
}

function normalizeString(str = "") {
  return str.toLowerCase().trim();
}

export function HomePage(props) {
  const {
    firstLoading,
    loadingMore,
    error,
    data = { pokemons: [] },
    getMorePokemon,
  } = useGetMorePokemon();
  const [searchValue, setSearchValue] = useState("");
  const normalizedSearchValue = useMemo(() => normalizeString(searchValue), [
    searchValue,
  ]);
  const filteredPokemons = useMemo(
    () =>
      data.pokemons.filter((pokemon) =>
        pokemon.name.includes(normalizedSearchValue)
      ),
    [normalizedSearchValue, data.pokemons]
  );
  if (firstLoading) return <Loading />;
  if (error) return <p>Error :(</p>;

  return (
    <div className={styles.container}>
      <img
        alt="logo"
        className={styles.logo}
        src={`${process.env.PUBLIC_URL}/pokemon-logo.png`}
      />
      <Search
        value={searchValue}
        onChange={({ target: { value } }) => setSearchValue(value)}
      />
      <PokemonList
        pokemons={filteredPokemons}
        {...props}
        getMorePokemon={getMorePokemon}
        loadingMore={loadingMore}
      />
    </div>
  );
}
