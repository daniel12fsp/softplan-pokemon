import { useQuery, useMutation } from "@apollo/react-hooks";
import React from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { useParams } from "react-router-dom";
import { Loading } from "../../components";
import { ExpandedCard } from "../../components/ExpandedCard/ExpandedCard";
import { GET_POKEMON, SAVE_POKEMON } from "../../graphQL/pokemon";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function handleToastSuccess() {
  toast.success("Pokemon Editado", {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function DescribePage(props) {
  const { id } = useParams();
  const { loading, error, data } = useQuery(GET_POKEMON, {
    variables: {
      id,
    },
  });

  const [onSavePokemon] = useMutation(SAVE_POKEMON);

  const onSavePokemonWithToast = (form) => {
    onSavePokemon(form);
    handleToastSuccess();
  };

  if (loading) return <Loading />;
  if (error) return <p>Error :(</p>;

  return (
    <>
      <ExpandedCard {...data.pokemon} onSavePokemon={onSavePokemonWithToast} />

      <ToastContainer />
    </>
  );
}
