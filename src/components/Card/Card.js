import React, { useCallback } from "react";
import styles from "./Card.module.css";
import { useHistory } from "react-router-dom";

function Text({ label, children }) {
  return (
    <div>
      <span>{label}</span>: <span>{children}</span>
    </div>
  );
}

export function Card({ id, name, image, onClick, types }) {
  const history = useHistory();
  const pokemonUrl = `/pokemons/${id}`;
  const goToPokemon = useCallback(() => history.push(pokemonUrl), [
    history,
    pokemonUrl,
  ]);
  return (
    <div className={styles.container} onClick={goToPokemon}>
      <img alt="pokemon" className={styles.image} src={image} />
      <div className={styles.texts}>
        <Text label="Nome">{name}</Text>

        {types && <Text label="Tipo">{types.join(", ")}</Text>}
      </div>
    </div>
  );
}
