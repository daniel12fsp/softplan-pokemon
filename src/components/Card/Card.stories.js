import React from "react";
import { Card } from "./Card";
import { action } from "@storybook/addon-actions";
import pokemon from "../../mocks/pokemon.json";

export default { title: "Card" };

export const defaultState = () => (
  <Card {...pokemon} onClick={action("clicked")} />
);
