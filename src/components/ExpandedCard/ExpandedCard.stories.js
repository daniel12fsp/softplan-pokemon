import React from "react";
import pokemon from "../../mocks/pokemon.json";
import { ExpandedCard } from "./ExpandedCard";

export default { title: "ExpandedCard" };

export const defaultState = () => <ExpandedCard {...pokemon} />;
