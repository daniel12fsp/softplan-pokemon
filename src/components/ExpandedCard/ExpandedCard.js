import React from "react";
import styles from "./ExpandedCard.module.css";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";

function Text({ name, label, value, edited, register }) {
  const renderValue = edited ? (
    <input name={name} defaultValue={value} ref={register} />
  ) : (
    <span className={styles.value}>{value}</span>
  );
  return (
    <div className={styles.text}>
      <span className={styles.label}>{label}</span>:{renderValue}
    </div>
  );
}

export function ExpandedCard({
  id,
  name,
  weight,
  height,
  classification,
  types,
  resistant,
  weaknesses,
  fleeRate,
  maxCP,
  maxHP,
  image,
  onSavePokemon,
}) {
  const { register, handleSubmit } = useForm();
  const handleSavePokemon = (data) => {
    const variables = {
      variables: {
        id,
        ...data,
      },
    };
    onSavePokemon(variables);
  };

  return (
    <>
      <Link to="/">Voltar</Link>
      <div
        className={styles.container}
        onSubmit={handleSubmit(handleSavePokemon)}
      >
        <img alt="pokemon" src={image} />
        <form>
          <Text
            label="Nome"
            name="name"
            edited={true}
            value={name}
            register={register}
          />

          <Text
            label="Peso mínimo"
            name="weightMinimum"
            edited={true}
            value={weight.minimum}
            register={register}
          />
          <Text
            label="Peso máximo"
            name="weightMaximum"
            edited={true}
            value={weight.maximum}
            register={register}
          />
          <Text
            label="Altura Mínimo"
            name="heightMinimum"
            edited={true}
            value={height.minimum}
            register={register}
          />
          <Text
            label="Altura máximo"
            name="heightMaximum"
            edited={true}
            value={height.maximum}
            register={register}
          />
          <Text
            label="Classificação"
            name="classification"
            edited={true}
            value={classification}
            register={register}
          />

          <Text
            label="Taxa de fulga"
            name="fleeRate"
            edited={true}
            value={fleeRate}
            register={register}
          />
          <Text
            label="Max. CP"
            name="maxCP"
            edited={true}
            value={maxCP}
            register={register}
          />
          <Text
            label="Max. HP"
            name="maxHP"
            edited={true}
            value={maxHP}
            register={register}
          />

          <Text label="Tipo" edited={false} value={types.join(", ")} />
          <Text
            label="Resistência"
            edited={false}
            value={resistant.join(", ")}
          />
          <Text
            label="Ponto fraco"
            edited={false}
            value={weaknesses.join(", ")}
          />
          <input type="submit" value="Salvar" />
        </form>
      </div>
    </>
  );
}
