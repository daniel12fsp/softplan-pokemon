import React from "react";
import styles from "./Loading.module.css";
import Loader from "react-loader-spinner";

export function Loading() {
  return (
    <div className={styles.container}>
      <Loader
        type="TailSpin"
        color="#00BFFF"
        height={100}
        width={100}
        label="audio-loading"
      />
    </div>
  );
}
