import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import styles from "./Search.module.css";
import cx from "classnames";

export function Search({ className, ...props }) {
  return (
    <div className={cx(styles.container, className)}>
      <div className={styles.inputWrapper}>
        <input className={styles.input} {...props} />

        <FontAwesomeIcon className={styles.icon} icon={faSearch} />
      </div>
    </div>
  );
}
