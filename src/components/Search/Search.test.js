import React from "react";

import { render, fireEvent } from "@testing-library/react";
import { Search } from "./Search";

const setup = (props) => {
  const utils = render(<Search {...props} />);
  const input = utils.container.querySelector("input");
  expect(input).not.toBe(null);

  return {
    input,
    ...utils,
  };
};

describe("<Search/> should", () => {
  it("Match snapshot", () => {
    const { asFragment } = setup();
    const firstRender = asFragment();
    expect(firstRender).toMatchSnapshot();
  });

  it("Should onChange works", () => {
    const onChangeMock = jest.fn();
    const { input } = setup({ onChange: onChangeMock });
    const event = { target: { value: "panda" } };
    fireEvent.change(input, event);
    expect(onChangeMock).toHaveBeenCalled();
  });

  it("Should className to passed in component", () => {
    const className = "myCustomClass";
    const { container, asFragment } = setup({ className });
    const classNameDiv = container.querySelector(`.${className}`);
    expect(classNameDiv).not.toBe(null);
  });
});
