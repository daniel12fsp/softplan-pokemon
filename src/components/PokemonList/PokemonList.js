import React from "react";
import styles from "./PokemonList.module.css";
import { Card } from "..";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { useInfiniteScroll } from "../../helpers/useInfiniteScroll";
import { Loading } from "../Loading/Loading";

export function PokemonList({
  pokemons,
  onClickCard,
  getMorePokemon,
  loadingMore,
}) {
  useInfiniteScroll(getMorePokemon);

  if (pokemons.length === 0) return "Não tem pokemon =(";

  return (
    <>
      <div className={styles.container}>
        {pokemons.map((pokemon) => (
          <Card
            key={pokemon.id}
            {...pokemon}
            onClick={() => onClickCard(pokemon)}
          />
        ))}
      </div>
      {loadingMore && <Loading />}
    </>
  );
}
