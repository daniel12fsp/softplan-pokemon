import React from "react";

import { render, fireEvent } from "@testing-library/react";
import { PokemonList } from "./PokemonList";
import { MockedPokemonList } from "./MockedPokemonList";
import { act } from "react-dom/test-utils";

export const setupPokemonList = (props) => {
  return render(<MockedPokemonList {...props} />);
};

describe("<PokemonList/> should", () => {
  it("Match snapshot", () => {
    const { asFragment } = setupPokemonList();
    const firstRender = asFragment();
    expect(firstRender).toMatchSnapshot();
  });

  it("Should  getMorePokemon be called", async () => {
    const mockedGetMorePokemon = jest.fn().mockResolvedValue(43);

    setupPokemonList({ getMorePokemon: mockedGetMorePokemon });

    expect(mockedGetMorePokemon).not.toHaveBeenCalled();
    await act(async () => {
      window.innerHeight = 0;

      fireEvent.scroll(window, { target: { scrollY: 300 } });
    });

    expect(mockedGetMorePokemon).toHaveBeenCalled();
  });
});
