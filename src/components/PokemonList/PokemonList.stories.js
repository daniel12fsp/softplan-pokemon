import React from "react";
import { MockedPokemonList } from "./MockedPokemonList";

export default { title: "PokemonList" };

export const defaultState = () => <MockedPokemonList />;
