import React from "react";
import { useMockPokemon } from "../../mocks/useMockPokemon";
import { PokemonList } from "./PokemonList";

export const MockedPokemonList = (props) => {
  const [pokemons, getMorePokemon] = useMockPokemon(20);
  return (
    <PokemonList
      pokemons={pokemons}
      getMorePokemon={getMorePokemon}
      {...props}
    />
  );
};
