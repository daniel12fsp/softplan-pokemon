import { useState } from "react";
import pokemon from "./pokemon.json";

export const generatePokemons = (n) =>
  new Array(n).fill(undefined).map((_, index) => ({ ...pokemon, id: index }));

export const useMockPokemon = () => {
  const [ammount, setAmmount] = useState(20);
  const [pokemons, setPokemons] = useState(generatePokemons(ammount));
  const getMorePokemon = () => {
    return new Promise((resolve, reject) => {
      const newAmmount = ammount + 20;
      setAmmount(newAmmount);
      setTimeout(() => {
        setPokemons(generatePokemons(newAmmount));
        resolve();
      }, 3000);
    });
  };

  return [pokemons, getMorePokemon];
};
