module.exports = {
  stories: ["../src/**/*.stories.js"],
  addons: [
    "@storybook/preset-create-react-app",
    "@storybook/addon-actions",
    "@storybook/addon-links",
  ],
  webpackFinal: (config) => {
    const root = process.cwd();

    const resetCssPath = `${root}/node_modules/reset-css/reset.css`;
    config.entry.push(resetCssPath);

    const themePath = `${root}/src/theme/theme.css`;
    config.entry.push(themePath);

    return config;
  },
};
